# dt-build: build live image based on Debian/Ubuntu

Bash script to create a live CD based on Debian/Ubuntu. Though Debian offers a
set of tools to do this already, this one has the advantage that is relatively
simple: provide a `buildfile`, an `overlay` directory and `dt-build` will set
it up for you, resulting in a `livecd.iso` which you can burn, run virtual,
`dd` to an USB and such.


## multi-distro

The Debian (specific) package `debootstrap` is luckily adopted by several
distributions, which results in `dt-build` being a multi-distro package.
It's been tested on (used a docker container) 
[centos:7](https://hub.docker.com/_/centos/?tab=tags&name=7) and 
[archlinux](https://hub.docker.com/_/archlinux/?tab=tags&name=latest).


## installation

Just copy/symlink the script `dt-build` (and `dt-chroot`) to a location part of
the PATH environment variable to make it execute when typed at any location.

example: `cp dt-build ~/.local/bin/`


## usage

```bash
12:00:00 checking requirements... [done]
Usage: ./dt-build [OPTION...]

  -D          drop unnecessary files (see --drop)
  -h, --help  this help information
  -v          verbose; shows command output

  --drop      list unnecessary files
  --noiso     do not build iso image
  --nosqfs    do not build squashfs filesystem
```

## build live cd

 * Create `buildfile`

```bash
cp buildfile{.example,}
vi buildfile
# set options to your likings
```

 * Create optional `overlay`

```bash
mkdir overlay
# add the required files including directory structures you require
```

 * Start "building" your `livecd.iso`

```bash
sudo su
dt-build -v
```


## quick test

If you have [qemu](https://www.qemu.org/) (and optionally
[kvm](https://wiki.qemu.org/Features/KVM)) locally available:

```bash
qemu-system-x86_64 -enable-kvm -m 256M -boot d -cdrom livecd.iso
```


### set root password

```bash
~# dt-build
12:07:29: checking requirements... [done]
12:07:29: debootstrap... [done]
12:08:59: add user... [done]
12:09:00: add overlay... [done]
12:09:00: install packages [+3]... [done]
12:10:04: add overlay... [done]
12:10:05: clean chroot... [done]
12:10:05: build squashfs... [done]
12:11:00: build iso image... [done]
12:11:01: finished
```

```bash
~# dt-chroot
chrooting into `/debian-base/chroot_amd64'...
~# passwd
Enter new UNIX password:
Retype new UNIX password:
passwd: password updated successfully
~# exit
```

```bash
~# dt-build
12:12:49: checking requirements... [done]
12:12:49: debootstrap... [ok]
12:12:49: add user... [done]
12:12:49: add overlay... [done]
12:12:49: install packages... [ok]
12:12:49: clean chroot... [done]
12:12:51: build squashfs... [done]
12:13:45: build iso image... [done]
12:13:47: finished
```
